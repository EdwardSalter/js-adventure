// TODO: CREATE A REDUCER FOR EACH SCREEN?

import actions from './actions'
import inventoryItems from './inventoryItems'
import gameFlags from './gameFlags'

const genericText = {
  noUse: 'You have nothing of immediate use to you.'
}

export default {
  screen1: {
    locationText: 'You are standing beside your truck ready to start the next leg of your journey. A road stretches away to the north and south as far as you can see.',
    actions: [
      {
        type: actions.enter,
        requirements: {
          gameFlags: [gameFlags.unlockTruck],
          unfulfilledText: 'You attempt to enter the truck but it is locked.',
          fulfilledText: 'You get in the truck and head north.'
        },
        screen: 'endgame'
      },
      {
        type: actions.use,
        item: inventoryItems.keys,
        requirements: {
          items: [inventoryItems.keys],
          unfulfilledText: genericText.noUse,
          fulfilledText: 'You press the keys in to the truck and unlock the door.'
        },
        setFlag: gameFlags.unlockTruck
      },
      { type: actions.north, screen: 'screen2' }
    ]
  },
  screen2: {
    locationText: 'You are stood beside a small wooden shack to the east overlooking the main road.',
    actions: [
      { type: actions.enter, screen: 'screen3' },
      { type: actions.east, screen: 'screen4' },
      { type: actions.south, screen: 'screen1' }
    ]
  },

  screen3: {
    locationText: 'You are standing inside a small wooden hut. The walls are dilapidated and falling apart. An angry looking parrot is stood on a table beside a shiny set of keys.',
    actions: [
      {
        type: actions.take,
        item: inventoryItems.keys,
        requirements: {
          gameFlags: [gameFlags.peanutsGivenToParrot],
          unfulfilledText: 'The parrot lunches at your hand as you try to reach for the keys and you withdraw it quickly.',
          fulfilledText: 'Now that the parrot is busy eating, you reach out and quickly grab the keys.'
        }
      },
      {
        type: actions.use,
        requirements: {
          items: [inventoryItems.peanuts],
          unfulfilledText: genericText.noUse,
          fulfilledText: 'You chuck the peanuts to the parrot and it starts to engulf them greedily.'
        },
        item: inventoryItems.peanuts,
        setFlag: gameFlags.peanutsGivenToParrot
      },
      { type: actions.exit, screen: 'screen2' }
    ]
  },

  screen4: {
    locationText: 'You are standing around the back of a crumbling wooden hut. Propped up against the wall is a sack of peanuts.',
    actions: [
      {
        type: actions.take,
        text: 'You grab a handful of peanuts and stuff them in your pocket.',
        item: inventoryItems.peanuts
      },
      {
        type: actions.west,
        screen: 'screen2'
      }
    ]
  },

  endgame: {
    locationText: 'You win! Congratulations.'
  }
}