/**
 * Check to see if requirements have been met
 * @param {Object} requirements
 * @param {String[]} [requirements.gameFlags]     A list of game flags that should be true
 * @param {String[]} [requirements.items]         A list of items that are required to be in the inventory
 * @param {Object} state                          The full redux state tree
 * @returns {boolean}                             True if all specified requirements have been met (or none specified), otherwise false
 */
export function haveRequirementsBeenMet (requirements, state) {
  const { gameFlags, items } = requirements

  // Check that all game flag requirements are met
  if (gameFlags) {
    const requirementsMet = gameFlags.every(flagName => state.gameFlags[flagName] === true)
    if (!requirementsMet) return false
  }

  // Check that all item requirements are met
  if (items) {
    const requirementsMet = items.every(i => state.inventory[i] != null)
    if (!requirementsMet) return false
  }

  return true
}