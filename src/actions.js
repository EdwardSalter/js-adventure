export const directions = { north: 'north', east: 'east', 'west': 'west', 'south': 'south' }
const actions = { ...directions, open: 'open', take: 'take', 'enter': 'enter', 'exit': 'exit', use: 'use' }

export default actions